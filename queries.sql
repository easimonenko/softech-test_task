select concat_ws(", ", AdminNameLevel1, AdminNameLevel2, AdminNameLevel3, AdminNameLevel4)
from ParsedAddress 
where address_id in (
  select id from AddressList where location_id in (
    select id from LocationList where place_id in (
      select place_id from CategoryList where CategoryId = 9567 and categorySystem="poi")));

select BaseText
from NameTextList
where name_id in (
  select id from NameList where place_id in (
    select id from PlaceList where id in (
      select place_id from LocationList where id in (
        select location_id from AddressList where id in (
          select address_id from ParsedAddress where CountryCode = "RUS")))))
  and textType = "OFFICIAL" and BaseText is not null and not BaseText = "";
