import defusedxml.ElementTree as ElementTree

# Handlers:

placeChildHandlers = {
  "Identity": lambda place, tag: identityHandler(place, tag),
  "LocationList": lambda place, tag: locationListHandler(place, tag),
  "ContactList": lambda place, tag: contactListHandler(place, tag),
  "NameList": lambda place, tag: nameListHandler(place, tag),
  "CategoryList": lambda place, tag: categoryListHandler(place, tag),
  "Unknown": lambda place, tag: unknownTagHandler(place, tag)
}

def identityHandler(place, tag):
  for child in tag:
    identityChildHandlers.get(child.tag, identityChildHandlers["Unknown"])(place, child)

identityChildHandlers = {
  "TimeStamp": lambda place, tag: timeStampHandler(place, tag),
  "PlaceId": lambda place, tag: placeIdHandler(place, tag),
  "Unknown": lambda place, tag: unknownTagHandler(place, tag)
}

def timeStampHandler(place, tag):
  place["TimeStamp"] = tag.text

def placeIdHandler(place, tag):
  place["PlaceId"] = tag.text

location_id = 0

def locationListHandler(place, tag):
  global location_id
  for child in tag:
    location_id += 1
    location = {"id": location_id, "place_id": place["id"]}
    locationListChildHandlers.get(child.tag, locationListChildHandlers["Unknown"])(location, child)
    locationListFile.write("\t".join(
      map(str, map(lambda x: location[x] if x in location else "", locationListFields))
    ))
    locationListFile.write("\n")

locationListChildHandlers = {
  "Location": lambda location, tag: locationHandler(location, tag),
  "Unknown": lambda location, tag: unknownTagHandler(location, tag)
}

def locationHandler(location, tag):
  location["locationId"] = tag.attrib["locationId"]
  for child in tag:
    locationChildHandlers.get(child.tag, locationChildHandlers["Unknown"])(location, child)

locationChildHandlers = {
  "Address": lambda location, tag: addressHandler(location, tag),
  "GeoPositionList": lambda location, tag: geoPositionListHandler(location, tag),
  "Unknown": lambda location, tag: unknownTagHandler(location, tag)
}

address_id = 0

def addressHandler(location, tag):
  global address_id
  address_id += 1
  address = {"id": address_id, "location_id": location["id"]}
  for child in tag:
    addressHandlers.get(child.tag, addressHandlers["Unknown"])(address, child)
  addressListFile.write("\t".join(
    map(str, map(lambda x: address[x] if x in address else "", addressListFields))
  ))
  addressListFile.write("\n")

addressHandlers = {
  "Parsed": lambda address, tag: parsedAddressHandler(address, tag),
  "Unknown": lambda address, tag: unknownTagHandler(address, tag)
}

parsedAddress_id = 0

def parsedAddressHandler(address, tag):
  global parsedAddress_id
  parsedAddress_id += 1
  parsedAddress = {"id": parsedAddress_id, "address_id": address["id"]}
  for attribute in tag.attrib:
    parsedAddressAttributeHandlers.get(attribute, parsedAddressAttributeHandlers["unknown"])(parsedAddress, tag, attribute)
  for child in tag:
    parsedAddressChildHandlers.get(child.tag, parsedAddressChildHandlers["Unknown"])(parsedAddress, child)
  parsedAddressFile.write("\t".join(
    map(str, map(lambda x: parsedAddress[x] if x in parsedAddress else "", parsedAddressFields))
  ))
  parsedAddressFile.write("\n")

parsedAddressAttributeHandlers = {
  "languageCode": lambda parsedAddress, tag, attribute: parsedAddressLanguageCodeHandler(parsedAddress, tag, attribute),
  "unknown": lambda parsedAddress, tag, attribute: unknownAttributeHandler(parsedAddress, tag, attribute)
}

def parsedAddressLanguageCodeHandler(parsedAddress, tag, attribute):
  parsedAddress[attribute] = tag.attrib[attribute]

parsedAddressChildHandlers = {
  "StreetName": lambda parsedAddress, tag: streetNameHandler(parsedAddress, tag),
  "HouseNumber": lambda parsedAddress, tag: houseNumberHandler(parsedAddress, tag),
  "CountryCode": lambda parsedAddress, tag: countryCodeHandler(parsedAddress, tag),
  "PostalCode": lambda parsedAddress, tag: postalCodeHandler(parsedAddress, tag),
  "Admin": lambda parsedAddress, tag: adminHandler(parsedAddress, tag),
  "Unknown": lambda parsedAddress, tag: unknownTagHandler(parsedAddress, tag)
}

def streetNameHandler(parsedAddress, tag):
  for child in tag:
    streetNameChildHandlers.get(child.tag, streetNameChildHandlers["Unknown"])(parsedAddress, child)

streetNameChildHandlers = {
  "BaseName": lambda parsedAddress, tag: streetBaseNameHandler(parsedAddress, tag),
  "StreetType": lambda parsedAddress, tag: streetTypeHandler(parsedAddress, tag),
  "Suffix": lambda parsedAddress, tag: suffixHandler(parsedAddress, tag),
  "Unknown": lambda parsedAddress, tag: unknownTagHandler(parsedAddress, tag)
}

def streetBaseNameHandler(parsedAddress, tag):
  parsedAddress["StreetBaseName"] = tag.text

def suffixHandler(parsedAddress, tag):
  parsedAddress["StreetNameSuffix"] = tag.text

def streetTypeHandler(parsedAddress, tag):
  for attribute in tag.attrib:
    streetTypeAttributeHandlers.get(attribute, streetTypeAttributeHandlers["unknown"])(parsedAddress, tag, attribute)
  parsedAddress["StreetType"] = tag.text

streetTypeAttributeHandlers = {
  "before": lambda parsedAddress, tag, attribute: streetTypeBeforeHandler(parsedAddress, tag, attribute),
  "unknown": lambda parsedAddress, tag, attribute: unknownAttributeHandler(parsedAddress, tag, attribute)
}

def streetTypeBeforeHandler(parsedAddress, tag, attribute):
  parsedAddress["StreetTypeBefore"] = tag.attrib[attribute]

def houseNumberHandler(parsedAddress, tag):
  parsedAddress["HouseNumber"] = tag.text

def countryCodeHandler(parsedAddress, tag):
  parsedAddress["CountryCode"] = tag.text

def postalCodeHandler(parsedAddress, tag):
  postalCode = tag.text
  parsedAddress["PostalCode"] = postalCode if postalCode != None else ""

def adminHandler(parsedAddress, tag):
  for child in tag:
    adminChildHandlers.get(child.tag, adminChildHandlers["Unknown"])(parsedAddress, child)

adminChildHandlers = {
  "AdminLevel": lambda parsedAddress, tag: adminLevelHandler(parsedAddress, tag),
  "AdminName": lambda parsedAddress, tag: adminNameHandler(parsedAddress, tag),
  "Unknown": lambda parsedAddress, tag: unknownTagHandler(parsedAddress, tag)
}

def adminLevelHandler(parsedAddress, tag):
  for child in tag:
    adminLevelChildHandlers.get(child.tag, adminLevelChildHandlers["Unknown"])(parsedAddress, child)

adminLevelChildHandlers = {
  "Level1": lambda parsedAddress, tag: adminLevel1Handler(parsedAddress, tag),
  "Level2": lambda parsedAddress, tag: adminLevel2Handler(parsedAddress, tag),
  "Level3": lambda parsedAddress, tag: adminLevel3Handler(parsedAddress, tag),
  "Level4": lambda parsedAddress, tag: adminLevel4Handler(parsedAddress, tag),
  "Unknown": lambda parsedAddress, tag: unknownTagHandler(parsedAddress, tag)
}

def adminLevel1Handler(parsedAddress, tag):
  parsedAddress["AdminLevel1"] = tag.text

def adminLevel2Handler(parsedAddress, tag):
  parsedAddress["AdminLevel2"] = tag.text

def adminLevel3Handler(parsedAddress, tag):
  parsedAddress["AdminLevel3"] = tag.text
  
def adminLevel4Handler(parsedAddress, tag):
  parsedAddress["AdminLevel4"] = tag.text

def adminNameHandler(parsedAddress, tag):
  for child in tag:
    adminNameChildHandlers.get(child.tag, adminNameChildHandlers["Unknown"])(parsedAddress, child)

adminNameChildHandlers = {
  "Level1": lambda parsedAddress, tag: adminNameLevel1Handler(parsedAddress, tag),
  "Level2": lambda parsedAddress, tag: adminNameLevel2Handler(parsedAddress, tag),
  "Level3": lambda parsedAddress, tag: adminNameLevel3Handler(parsedAddress, tag),
  "Level4": lambda parsedAddress, tag: adminNameLevel4Handler(parsedAddress, tag),
  "Unknown": lambda parsedAddress, tag: unknownTagHandler(parsedAddress, tag)
}

def adminNameLevel1Handler(parsedAddress, tag):
  parsedAddress["AdminNameLevel1"] = tag.text

def adminNameLevel2Handler(parsedAddress, tag):
  parsedAddress["AdminNameLevel2"] = tag.text

def adminNameLevel3Handler(parsedAddress, tag):
  parsedAddress["AdminNameLevel3"] = tag.text

def adminNameLevel4Handler(parsedAddress, tag):
  parsedAddress["AdminNameLevel4"] = tag.text

def geoPositionListHandler(location, tag):
  for child in tag:
    geoPositionListHandlers.get(child.tag, geoPositionListHandlers["Unknown"])(location, child)

geoPositionListHandlers = {
  "GeoPosition": lambda location, tag: geoPositionHandler(location, tag),
  "Unknown": lambda location, tag: unknownTagHandler(location, tag)
}

geoPosition_idx = 0
def geoPositionHandler(location, tag):
  global geoPosition_idx
  geoPosition_idx += 1
  geoPosition = {"id": geoPosition_idx, "location_id": location["id"]}
  for child in tag:
    geoPositionHandlers.get(child.tag, geoPositionHandlers["Unknown"])(geoPosition, child)
  if not "Altitude" in geoPosition.keys():
     geoPosition["Altitude"] = ""
  geoPositionListFile.write("\t".join(map(str, [geoPosition["id"], geoPosition["location_id"], geoPosition["Latitude"], geoPosition["Longitude"], geoPosition["Altitude"]])))
  geoPositionListFile.write("\n")

geoPositionHandlers = {
  "Latitude": lambda geoPosition, tag: latitudeHandler(geoPosition, tag),
  "Longitude": lambda geoPosition, tag: longitudeHandler(geoPosition, tag),
  "Altitude": lambda geoPosition, tag: altitudeHandler(geoPosition, tag),
  "Unknown": lambda geoPosition, tag: unknownTagHandler(geoPosition, tag)
}

def latitudeHandler(geoPosition, tag):
  geoPosition["Latitude"] = tag.text

def longitudeHandler(geoPosition, tag):
  geoPosition["Longitude"] = tag.text

def altitudeHandler(geoPosition, tag):
  geoPosition["Altitude"] = tag.text

def contactListHandler(place, tag):
  for child in tag:
    contactListHandlers.get(child.tag, contactListHandlers["Unknown"])(place, child)

contactListHandlers = {
  "Contact": lambda place, tag: contactHandler(place, tag),
  "Unknown": lambda place, tag: unknownTagHandler(place, tag)
}

contact_idx = 0
def contactHandler(place, tag):
  global contact_idx
  contact_idx += 1
  contact = {"id": contact_idx, "place_id": place["id"]}
  for attribute in tag.attrib:
    contactAttributeHandlers.get(attribute, contactAttributeHandlers["unknown"])(contact, tag, attribute)
  for child in tag:
    contactHandlers.get(child.tag, contactHandlers["Unknown"])(contact, child)
  contactListFile.write("\t".join(map(str, [contact["id"], contact["place_id"], contact["ContactType"], contact["ContactString"]])))
  contactListFile.write("\n")

contactAttributeHandlers = {
  "type": lambda contact, tag, attribute: contactTypeHandler(contact, tag, attribute),
  "unknown": lambda contact, tag, attribute: unknownAttributeHandler(contact, tag, attribute)
}

def contactTypeHandler(contact, tag, attribute):
  contact["ContactType"] = tag.attrib["type"]

contactHandlers = {
  "ContactString": lambda contact, tag: contactStringHandler(contact, tag),
  "Unknown": lambda contact, tag: unknownTagHandler(contact, tag)
}

def contactStringHandler(contact, tag):
  contact["ContactString"] = tag.text

def nameListHandler(place, tag):
  for child in tag:
    nameListHandlers.get(child.tag, nameListHandlers["Unknown"])(place, child)

nameListHandlers = {
  "Name": lambda place, tag: nameHandler(place, tag),
  "Unknown": lambda place, tag: unknownTagHandler(place, tag)
}

name_idx = 0
def nameHandler(place, tag):
  global name_idx
  name_idx += 1
  name = {"id": name_idx, "place_id": place["id"]}
  for child in tag:
    nameHandlers.get(child.tag, nameHandlers["Unknown"])(name, child)
  nameListFile.write("\t".join(map(str, [name["id"], name["place_id"]])))
  nameListFile.write("\n")

nameHandlers = {
  "TextList": lambda name, tag: nameTextListHandler(name, tag),
  "Unknown": lambda name, tag: unknownTagHandler(name, tag)
}

def nameTextListHandler(name, tag):
  for child in tag:
    nameTextListHandlers.get(child.tag, nameTextListHandlers["Unknown"])(name, child)

nameTextListHandlers = {
  "Text": lambda name, tag: nameTextHandler(name, tag),
  "Unknown": lambda name, tag: unknownTagHandler(name, tag)
}

nameTextId = 0
def nameTextHandler(name, tag):
  global nameTextId
  nameTextId += 1
  nameText = {"id": nameTextId, "name_id": name["id"]}
  for child in tag:
    nameTextHandlers.get(child.tag, nameTextHandlers["Unknown"])(nameText, child)
  nameTextListFile.write("\t".join(map(str, [nameText["id"], nameText["name_id"], nameText["textType"], nameText["languageCode"], nameText["BaseText"]])))
  nameTextListFile.write("\n")

nameTextHandlers = {
  "BaseText": lambda nameText, tag: baseTextHandler(nameText, tag),
  "Unknown": lambda nameText, tag: unknownTagHandler(nameText, tag)
}

def baseTextHandler(nameText, tag):
  for attribute in tag.attrib:
    baseTextAttributeHandlers.get(attribute, baseTextAttributeHandlers["unknown"])(nameText, tag, attribute)
  nameText["BaseText"] = tag.text

baseTextAttributeHandlers = {
  "type": lambda nameText, tag, attribute: baseTextTypeHandler(nameText, tag, attribute),
  "languageCode": lambda nameText, tag, attribute: baseTextLanguageCodeHandler(nameText, tag, attribute),
  "unknown": lambda nameText, tag, attribute: unknownAttributeHandler(nameText, tag, attribute)
}

def baseTextTypeHandler(nameText, tag, attribute):
  types = ["OFFICIAL", "EXONYM", "SYNONYM", "ABBREVIATION"]
  if not tag.attrib[attribute] in types:
    raise Exception("Unknown type \'" + tag.attrib[attribute] + "\' in tag <" + tag.tag + ">")
  nameText["textType"] = tag.attrib[attribute]

baseTextLanguageCodeSet = dict()
def baseTextLanguageCodeHandler(nameText, tag, attribute):
  value = tag.attrib[attribute]
  nameText[attribute] = value
  if value in baseTextLanguageCodeSet:
    baseTextLanguageCodeSet[value] += 1
  else:
    baseTextLanguageCodeSet[value] = 1

def categoryListHandler(place, tag):
  for child in tag:
    categoryListHandlers.get(child.tag, categoryListHandlers["Unknown"])(place, child)
    
categoryListHandlers = {
  "Category": lambda place, tag: categoryHandler(place, tag),
  "Unknown": lambda place, tag: unknownTagHandler(place, tag)
}

categoryId = 0
def categoryHandler(place, tag):
  global categoryId
  categoryId += 1
  category = {"id": categoryId, "place_id": place["id"]}
  for attribute in tag.attrib:
    categoryAttributeHandlers.get(attribute, categoryAttributeHandlers["unknown"])(category, tag, attribute)
  for child in tag:
    categoryHandlers.get(child.tag, categoryHandlers["Unknown"])(category, child)
  categoryListFile.write("\t".join(map(str, [category["id"], category["place_id"], category["categorySystem"], category["CategoryId"]])))
  categoryListFile.write("\n")

categoryAttributeHandlers = {
  "categorySystem": lambda category, tag, attribute: categorySystemHandler(category, tag, attribute),
  "unknown": lambda category, tag, attribute: unknownAttributeHandler(category, tag, attribute)
}

def categorySystemHandler(category, tag, attribute):
  category[attribute] = tag.attrib[attribute]

categoryHandlers = {
  "CategoryId": lambda category, tag: categoryIdHandler(category, tag),
  "CategoryName": lambda category, tag: categoryNameHandler(category, tag),
  "Unknown": lambda category, tag: unknownTagHandler(category, tag)
}

categoryIdSet = dict()
def categoryIdHandler(category, tag):
  value = tag.text
  category["CategoryId"] = value
  if value in categoryIdSet:
    categoryIdSet[value] += 1
  else:
    categoryIdSet[value] = 1

def categoryNameHandler(category, tag):
  for child in tag:
    categoryNameHandlers.get(child.tag, categoryNameHandlers["Unknown"])(category, child)

categoryNameHandlers = {
  "Text": lambda category, tag: categoryNameTextHandler(category, tag),
  "Unknown": lambda category, tag: unknownTagHandler(category, tag)
}

categoryNameTextId = 0
def categoryNameTextHandler(category, tag):
  global categoryNameTextId
  categoryNameTextId += 1
  categoryNameText = {"id": categoryNameTextId, "category_id": category["id"]}
  for attribute in tag.attrib:
    categoryNameTextAttributeHandler.get(attribute, categoryNameTextAttributeHandler["unknown"])(categoryNameText, tag, attribute)
  categoryNameText["NameText"] = tag.text
  categoryNameTextListFile.write("\t".join(map(str, [categoryNameText["id"], categoryNameText["category_id"], categoryNameText["languageCode"], categoryNameText["NameText"]])))
  categoryNameTextListFile.write("\n")

categoryNameTextAttributeHandler = {
  "languageCode": lambda categoryNameText, tag, attribute: categoryNameTextLanguageCodeHandler(categoryNameText, tag, attribute),
  "unknown": lambda categoryNameText, tag, attribute: unknownAttributeHandler(categoryNameText, tag, attribute)
}

def categoryNameTextLanguageCodeHandler(categoryNameText, tag, attribute):
  categoryNameText[attribute] = tag.attrib[attribute]

# Main:

def unknownTagHandler(node, tag):
  raise Exception("Unknown tag <" + tag.tag + ">")

def unknownAttributeHandler(node, tag, attribute):
  raise Exception("Unknown attribute \'" + attribute + "\' in tag <" + tag.tag + ">")

tree = ElementTree.parse("docs/sample.xml")
root = tree.getroot()
if root.tag != "PlaceList":
  raise Exception("Unknown root tag <" + "root.tag" + "> instead of <PlaceList>")

placeListFields = [
  "id", "TimeStamp", "PlaceId"
]
placeListFile = open("db/PlaceList.csv", "w")
placeListFile.write("\t".join(placeListFields))
placeListFile.write("\n")

locationListFields = [
  "id", "place_id", "locationId"
]
locationListFile = open("db/LocationList.csv", "w")
locationListFile.write("\t".join(locationListFields))
locationListFile.write("\n")

addressListFields = [
  "id", "location_id"
]
addressListFile = open("db/AddressList.csv", "w")
addressListFile.write("\t".join(addressListFields))
addressListFile.write("\n")

parsedAddressFields = [
  "id", "address_id", "languageCode", "StreetBaseName", "StreetType",
  "StreetTypeBefore", "StreetNameSuffix", "HouseNumber", "CountryCode",
  "PostalCode",
  "AdminLevel1", "AdminLevel2", "AdminLevel3", "AdminLevel4",
  "AdminNameLevel1", "AdminNameLevel2", "AdminNameLevel3", "AdminNameLevel4",
]
parsedAddressFile = open("db/ParsedAddress.csv", "w")
parsedAddressFile.write("\t".join(parsedAddressFields))
parsedAddressFile.write("\n")

geoPositionListFields = [
  "id", "location_id", "Latitude", "Longtitude", "Altitude"
]
geoPositionListFile = open("db/GeoPositionList.csv", "w")
geoPositionListFile.write("\t".join(geoPositionListFields))
geoPositionListFile.write("\n")

contactListFields = [
  "id", "place_id", "ContactType", "ContactString"
]
contactListFile = open("db/ContactList.csv", "w")
contactListFile.write("\t".join(contactListFields))
contactListFile.write("\n")

nameListFields = [
  "id", "place_id"
]
nameListFile = open("db/NameList.csv", "w")
nameListFile.write("\t".join(nameListFields))
nameListFile.write("\n")

nameTextListFields = [
  "id", "name_id", "textType", "languageCode", "BaseText"
]
nameTextListFile = open("db/NameTextList.csv", "w")
nameTextListFile.write("\t".join(nameTextListFields))
nameTextListFile.write("\n")

categoryListFields = [
  "id", "place_id", "categorySystem", "CategoryId"
]
categoryListFile = open("db/CategoryList.csv", "w")
categoryListFile.write("\t".join(categoryListFields))
categoryListFile.write("\n")

categoryNameTextListFields = [
  "id", "category_id", "languageCode", "NameText"
]
categoryNameTextListFile = open("db/CategoryNameTextList.csv", "w")
categoryNameTextListFile.write("\t".join(categoryNameTextListFields))
categoryNameTextListFile.write("\n")

placeStatisticsFile = open("db/PlaceStatistics.csv", "w")
categoryIdStatisticsFile = open("db/CategoryIdStatistics.csv", "w")
baseTextLanguageCodeStatisticsFile = open("db/BaseTextLanguageCodeStatistics.csv", "w")

place_idx = 0
for placeTag in root:
  if placeTag.tag != "Place":
    raise "Unknown tag <" + placeTag.tag + "> in <" + root.tag + ">"
  
  place_idx += 1
  place = {"id": place_idx}
  
  for child in placeTag:
    placeChildHandlers.get(child.tag, placeChildHandlers["Unknown"])(place, child)
  
  placeListFile.write("\t".join(map(str, map(lambda x: place[x], placeListFields))))
  placeListFile.write("\n")

for k in baseTextLanguageCodeSet:
  baseTextLanguageCodeStatisticsFile.write(str(k) + ":" + str(baseTextLanguageCodeSet[k]))
  baseTextLanguageCodeStatisticsFile.write("\n")
baseTextLanguageCodeStatisticsFile.close()

for k in categoryIdSet:
  categoryIdStatisticsFile.write(str(k) + ":" + str(categoryIdSet[k]))
  categoryIdStatisticsFile.write("\n")
categoryIdStatisticsFile.close()

placeStatisticsFile.write(str(place_idx))
placeStatisticsFile.write("\n")
placeStatisticsFile.close()

categoryNameTextListFile.close()
categoryListFile.close()
nameTextListFile.close()
nameListFile.close()
contactListFile.close()
geoPositionListFile.close()
parsedAddressFile.close()
addressListFile.close()
locationListFile.close()
placeListFile.close()
