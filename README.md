# Тестовое задание от компании Программные технологии

Описание задания в docs/test_task_-_places.doc, пример входных данных в
docs/sample.xml.

## Установка

В Ubuntu нужно установить пакеты:

```
python3, pylint3, mysql-workbench, mysql-server, python3-defusedxml
```

В качестве среды разработки на Python использовалась Visual Studio Code 1.1. В 
Ubuntu её можно установить, скачав соответствующий пакет с 
[https://code.visualstudio.com/Download](https://code.visualstudio.com/Download)
. Также в Code нужно установать расширение для Python.

## ERP-модель

ERP-модель подготовлена в среде MySQL Workbench, файл model.mwb. Модель в разных
форматах для просмотра (png, svg, pdf) располагается в каталоге db. Там же находится 
сгенерированный из модели SQL-скрипт для создания базы данных.

## Скрипты

- xml2csv.py: транслирует исходный XML файл в набор CSV-файлов.
- check.py: программа проверки качества трансляции.

## База данных

Производные файлы базы данных после трансляции из XML располагаются в каталоге 
db. Там же располагаются производные файлы модели.

## CSV-файлы

Для извлечения данных из XML-файла в набор CVS-файлов нужно выполнить команду:

``` bash
python3 xml2csv.py
```

Полученные из исходного XML файлы в формате CSV располагаются в каталоге db:

- PlaceList
- LocationList
- GeoPositionList
- AddressList
- ParsedAddress
- ContactList
- NameList
- NameTextList
- CategoryList
- CategoryNameTextList

Импорт всех таблиц из CSV-файлов осуществляется командами:

``` bash
mysqlimport -u admin -p --local --ignore-lines 1 --verbose softech CSV-файл
```

Порядок задания CSV-файлов совпадает с вышеприведённым списком файлов CSV.

## Файлы статистики

Статистика находится в нескольких файлах, так как непонятно, как можно совместить разнородную информацию в одном CSV-файле.

Это файлы:

- PlaceStatistics.csv -- количество обработанных тегов Place.
- BaseTextLanguageCodeStatistics.csv -- уникальные значения атрибута languageCode, и сколько раз каждое из них встречается.
- CategoryIdStatistics.csv -- уникальные значения тега CategoryId, и сколько раз каждое из них встречается.

## Запросы SQL

Запросы SQL сохранены в файле queries.sql.

## Программа проверки

Программа проверки check.py. Находит ответы иным способом нежели xml2csv.py и сравнивает их с тем, что записано в соответствующих файлах статистики.

## Тест-кейсы

Увы, но какими должны быть тест-кейсы в контексте пункта 6 задания мне неясно, поэтому эта часть невыполнена.

---

(c) Евгений А. Симоненко, 2016
