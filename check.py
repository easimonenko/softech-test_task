import defusedxml.ElementTree as ElementTree

tree = ElementTree.parse("docs/sample.xml")
root = tree.getroot()
if root.tag != "PlaceList":
  raise Exception("Unknown root tag <" + "root.tag" + "> instead of <PlaceList>")

placeStatisticsFile = open("db/PlaceStatistics.csv", "r")
print(len(root.findall('./Place')) == int(placeStatisticsFile.read()))
placeStatisticsFile.close()

categoryIdStatisticsFile = open("db/CategoryIdStatistics.csv", "r")
lines = 0
for line in categoryIdStatisticsFile:
  lines += 1
elems = set()
for elem in root.findall('./Place/CategoryList/Category/CategoryId'):
  elems.add(elem.text)
print(len(elems) == lines)
categoryIdStatisticsFile.close()
